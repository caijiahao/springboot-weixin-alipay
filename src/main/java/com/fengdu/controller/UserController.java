package com.fengdu.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fengdu.entity.User;
import com.fengdu.service.UserService;

/**
 * 
* <p>Title: UserController.java</p>  
* <p>Description: </p>  
* <p>Copyright: Copyright (c) 2018</p>  
* @author tiankong 
* @email 2366207000@qq.com
* @date 2018年6月26日  
* @version 1.0
 */
@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/list")
	public Object products(Map<String, Object> map) throws Exception {
//		Map<String, Object> map=new HashMap<String, Object>();
		List<User> userList = userService.getUserList();
		map.put("userList", userList);
		return "template/user";
	}
	
}
